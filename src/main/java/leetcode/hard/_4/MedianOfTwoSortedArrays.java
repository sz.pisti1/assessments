package leetcode.hard._4;

/**
 * Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
 * The overall run time complexity should be O(log (m+n)).
 */
public class MedianOfTwoSortedArrays {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int fullLength = nums1.length + nums2.length;
        boolean isOdd = fullLength % 2 != 0;
        int signalIndex = isOdd ? fullLength / 2 : fullLength / 2 - 1;

        var i = 0;
        var j = 0;
        var current = 0;
        var next = 0;
        while (signalIndex-- >= 0) {
            current = getElement(nums1, i) <= getElement(nums2, j)
                    ? getElement(nums1, i++)
                    : getElement(nums2, j++);
            next = Math.min(
                    getElement(nums1, i),
                    getElement(nums2, j)
            );
        }

        return isOdd ? current : (double) (current + next) / 2;
    }

    private int getElement(int[] nums, int i) {
        return i >= nums.length || i < 0 ? Integer.MAX_VALUE : nums[i];
    }

}
