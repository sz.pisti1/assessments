package leetcode.easy._1;

import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * You can return the answer in any order.
 */
public class TwoSumV1 {

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> visited = new HashMap<>();
        for (var i = 0 ; i < nums.length ; i++) {
            if (visited.containsKey(target - nums[i])) {
                return new int[]{visited.get(target - nums[i]), i};
            } else {
                visited.put(nums[i], i);
            }
        }
        return new int[]{};
    }

}
