package leetcode.medium._2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order,
 * and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 */
public class AddTwoNumbersV1 {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        return addRec(l1, l2, false);
    }

    private ListNode addRec(ListNode l1, ListNode l2, boolean hasCarryOver) {
        if (l1 == null && l2 == null) return hasCarryOver ? new ListNode(1) : null;

        int sum = (l1 == null ? 0 : l1.val) + (l2 == null ? 0 : l2.val) + (hasCarryOver ? 1 : 0);

        return new ListNode(
                sum % 10,
                addRec(l1 == null ? null : l1.next, l2 == null ? null : l2.next, sum >= 10)
        );
    }

    @Data
    @Accessors(chain = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int val) { this.val = val; }
    }

}
