package leetcode.medium._8;

/**
 * Implement the myAtoi(string s) function, which converts a string to a 32-bit signed integer (similar to C/C++'s atoi function).
 */
public class StringToInteger {

    public int myAtoi(String s) {
        if (s == null || s.length() < 1) return 0;

        Boolean isSigned = null;
        var builder = new StringBuilder();
        for (var i = 0 ; i < s.length() ; i++) {
            var c = s.charAt(i);
            if (builder.length() != 0 || isSigned != null || c != 32) { //non-leading white spaces
                if (builder.length() == 0 && isSigned == null && c == 45) {
                    isSigned = true;
                } else if (builder.length() == 0 && isSigned == null && c == 43) {
                    isSigned = false;
                } else {
                    if (c >= 48 && c <= 57) {
                        builder.append(c);
                    } else {
                        break;
                    }
                }
            }
        }

        isSigned = isSigned != null && isSigned;
        try {
            var result = builder.length() == 0 ? 0 : Integer.parseInt(builder.toString());
            return isSigned ? result * -1 : result;
        } catch (NumberFormatException e) {
            return isSigned ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        }
    }

}
