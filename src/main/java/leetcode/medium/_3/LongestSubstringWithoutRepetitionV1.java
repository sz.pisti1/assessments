package leetcode.medium._3;

/**
 * Given a string s, find the length of the longest substring without repeating characters.
 */
public class LongestSubstringWithoutRepetitionV1 {

    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) return 0;
        if (s.length() == 1) return 1;

        var length = 0;
        var currentLongest = new StringBuilder();

        for (var i = 0 ; i < s.length() ; i++) {
            var currentChar = s.charAt(i);
            int currentCharIndex = currentLongest.toString().indexOf(currentChar);

            if (currentCharIndex != -1) {
                currentLongest.delete(0, currentCharIndex + 1);
            }

            currentLongest.append(currentChar);
            length = Math.max(length, currentLongest.length());
        }

        return length;
    }

}
