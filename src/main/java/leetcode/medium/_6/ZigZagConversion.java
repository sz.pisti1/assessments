package leetcode.medium._6;

/**
 * The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)
 * And then read line by line: "PAHNAPLSIIGYIR"
 * Write the code that will take a string and make this conversion given a number of rows.
 */
public class ZigZagConversion {
    public String convert(String s, int numRows) {
        if (s == null || s.length() < 1) return "";
        if (numRows == 1) return s;

        var result = new char[s.length()];

        var stepper1 = numRows + numRows - 2;
        var stepper2 = 0;
        var index = 0;
        for (var i = 0 ; i < numRows && index < s.length() ; i++) {
            var j = i;
            result[index++] = s.charAt(j);
            while (index < s.length()) {
                if (stepper1 != 0) {
                    j += stepper1;
                    if (j >= s.length()) break;
                    result[index++] = s.charAt(j);
                }
                if (stepper2 != 0) {
                    j += stepper2;
                    if (j >= s.length()) break;
                    result[index++] = s.charAt(j);
                }
            }
            stepper1 -= 2;
            stepper2 += 2;
        }

        return new String(result);
    }

}
