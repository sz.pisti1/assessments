package leetcode.medium._5;

/**
 * Given a string s, return the longest palindromic substring in s.
 */
public class LongestPalindromicSubstring {

        public String longestPalindrome(String s) {
            if (s == null || s.length() < 1) return "";

            var start = 0;
            var end = 0;

            for (var i = 0 ; i < s.length() ; i++) {
                var len = Math.max(expandFromCenter(s, i, i), expandFromCenter(s, i, i + 1));
                if (len > end - start) {
                    start = i - ((len - 1) / 2);
                    end = i + (len / 2);
                }
            }

            return s.substring(start, end + 1);
        }

        private int expandFromCenter(String s, int left, int right) {
            while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
                left--;
                right++;
            }

            return right - left - 1;
        }

}
