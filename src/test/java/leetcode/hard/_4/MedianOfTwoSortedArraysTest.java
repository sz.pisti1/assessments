package leetcode.hard._4;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class MedianOfTwoSortedArraysTest {

    private static Stream<Arguments> provideInput() {
        return Stream.of(
                Arguments.of(new int[]{1, 3}, new int[]{2}, 2.0),
                Arguments.of(new int[]{1, 2}, new int[]{3, 4}, 2.5),
                Arguments.of(new int[]{0, 0}, new int[]{0, 0}, 0.0),
                Arguments.of(new int[]{}, new int[]{1}, 1.0),
                Arguments.of(new int[]{2}, new int[]{}, 2.0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideInput")
    void shouldFindIndexes(int[] nums1, int[] nums2, double expectedResult) {
        assertThat(new MedianOfTwoSortedArrays().findMedianSortedArrays(nums1, nums2))
                .isEqualTo(expectedResult);
    }

}
