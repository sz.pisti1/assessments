package leetcode.medium._5;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class LongestPalindromicSubstringTest {

    private static Stream<Arguments> provideInput() {
        return Stream.of(
                Arguments.of("babad", new Object[]{"bab", "aba"}),
                Arguments.of("cbbd", new Object[]{"bb"}),
                Arguments.of("a", new Object[]{"a"}),
                Arguments.of("ac", new Object[]{"a", "c"})
        );
    }

    @ParameterizedTest
    @MethodSource("provideInput")
    void shouldFindIndexes(String s, Object[] expectedResult) {
        assertThat(new LongestPalindromicSubstring().longestPalindrome(s))
                .isIn(expectedResult);
    }

}
