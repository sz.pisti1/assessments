package leetcode.medium._8;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class StringToIntegerTest {

    private static Stream<Arguments> provideInput() {
        return Stream.of(
                Arguments.of("42", 42),
                Arguments.of("   -42", -42),
                Arguments.of("4193 with words", 4193),
                Arguments.of("words and 987", 0),
                Arguments.of("-91283472332", Integer.MIN_VALUE),
                Arguments.of("+1", 1),
                Arguments.of("+-12", 0),
                Arguments.of("  +  413", 0),
                Arguments.of("3.14159", 3),
                Arguments.of("20000000000000000000", Integer.MAX_VALUE),
                Arguments.of("abc", 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideInput")
    void shouldFindIndexes(String input, int expectedResult) {
        assertThat(new StringToInteger().myAtoi(input))
                .isEqualTo(expectedResult);
    }

}
