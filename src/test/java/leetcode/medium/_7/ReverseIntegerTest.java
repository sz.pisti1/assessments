package leetcode.medium._7;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ReverseIntegerTest {

    private static Stream<Arguments> provideInput() {
        return Stream.of(
                Arguments.of(123, 321),
                Arguments.of(-123, -321),
                Arguments.of(120, 21),
                Arguments.of(0, 0),
                Arguments.of(-2147483648, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideInput")
    void shouldFindIndexes(int input, int expectedResult) {
        assertThat(new ReverseInteger().reverse(input))
                .isEqualTo(expectedResult);
    }

}
