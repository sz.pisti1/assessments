package leetcode.medium._2;

import leetcode.medium._2.AddTwoNumbersV1.ListNode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class AddTwoNumbersTest {

    private static Stream<Arguments> provideInput() {
        return Stream.of(
                Arguments.of(new int[]{2, 4, 3}, new int[]{5, 6, 4}, new int[]{7, 0, 8}),
                Arguments.of(new int[]{0}, new int[]{0}, new int[]{0}),
                Arguments.of(new int[]{9, 9, 9, 9, 9, 9, 9}, new int[]{9, 9, 9, 9}, new int[]{8, 9, 9, 9, 0, 0, 0, 1})
        );
    }

    @ParameterizedTest
    @MethodSource("provideInput")
    void shouldFindIndexes(int[] l1, int[] l2, int[] expectedResult) {
        assertThat(new AddTwoNumbersV1().addTwoNumbers(toListNode(l1), toListNode(l2)))
                .isEqualTo(toListNode(expectedResult));
    }

    private ListNode toListNode(int[] input) {
        ListNode result = new ListNode(input[0]);
        ListNode temp = result;
        for (var i = 0 ; i < input.length - 1 ; i++) {
            temp = temp.setNext(new ListNode(input[i + 1]));
            temp = temp.getNext();
        }

        return result;
    }

}
