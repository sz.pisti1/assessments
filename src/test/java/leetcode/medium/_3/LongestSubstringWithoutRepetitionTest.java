package leetcode.medium._3;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class LongestSubstringWithoutRepetitionTest {

    private static Stream<Arguments> provideInput() {
        return Stream.of(
                Arguments.of("abcabcbb", 3),
                Arguments.of("bbbbb", 1),
                Arguments.of("pwwkew", 3),
                Arguments.of("", 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideInput")
    void shouldFindIndexes(String input, int expectedResult) {
        assertThat(new LongestSubstringWithoutRepetitionV1().lengthOfLongestSubstring(input))
                .isEqualTo(expectedResult);
    }

}
