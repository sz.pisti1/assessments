package leetcode.medium._6;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ZigZagConversionTest {

    private static Stream<Arguments> provideInput() {
        return Stream.of(
                Arguments.of("PAYPALISHIRING", 3, "PAHNAPLSIIGYIR"),
                Arguments.of("PAYPALISHIRING", 4, "PINALSIGYAHRPI"),
                Arguments.of("A", 1, "A"),
                Arguments.of("A", 2, "A"),
                Arguments.of("ABCDE", 4, "ABCED")
        );
    }

    @ParameterizedTest
    @MethodSource("provideInput")
    void shouldFindIndexes(String s, int numRows, String expectedResult) {
        assertThat(new ZigZagConversion().convert(s, numRows))
                .isEqualTo(expectedResult);
    }

}
